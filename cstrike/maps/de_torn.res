////////////////////////////////////////////////////////////////
// de_torn resources file
//
// � Chapo 2004 - Infos @ http://www.17buddies.net
//
// Can you play all of our maps?? Sure not!!
//

//////Wads
\half-life\valve\torntextures.wad

//////Skies
gfx/env/tornskyup.tga
gfx/env/tornskydn.tga
gfx/env/tornskylf.tga
gfx/env/tornskyrt.tga
gfx/env/tornskyft.tga
gfx/env/tornskybk.tga

//////Sounds
sound/de_torn/torn_templewind.wav
sound/de_torn/tk_windstreet.wav
sound/ambience/burning1.wav
sound/ambience/cricket.wav
sound/ambience/jetflyby1.wav
sound/de_torn/tk_steam.wav
sound/de_torn/torn_ambience.wav
sound/ambience/copter.wav
sound/de_torn/torn_water1.wav
sound/de_torn/torn_water2.wav
sound/de_torn/torn_thndrstrike.wav
sound/de_torn/torn_ak-47.wav
sound/de_torn/torn_bomb2.wav
sound/de_torn/torn_bomb1.wav
sound/de_torn/torn_mgun1.wav
sound/ambience/drips.wav

//////Graphics

//////Sprites
sprites/glow04.spr
sprites/xssmke1.spr
sprites/xffloor.spr
sprites/xsmoke1.spr

//////Models

//////Misc
// Overview : no
// Waypoints: no
