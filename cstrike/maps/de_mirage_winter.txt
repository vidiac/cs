Mirage - Bomb/Defuse
by BubkeZ (bubkez@hotmail.com)
Winter Texture by Shur1k (cswar.net)
Counter-Terrorists: Prevent Terrorists
from bombing explosive crates.
Team members must defuse any bombs 
that threaten targeted areas.

Terrorists: The Terrorist carrying the
C4 must destroy one of the explosive 
crate areas.

Other Notes: There are 2 bomb targets
in the mission.


(Press FIRE to continue)
