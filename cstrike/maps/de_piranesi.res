////////////////////////////////////////////////////////////////
// de_piranesi resources file
//
// � Chapo 2004 - Infos @ http://www.17buddies.net
//
// Can you play all of our maps?? Sure not!!
//

//////Wads
\half-life\valve\halflife.wad
\half-life\cstrike\decals.wad
\half-life\valve\liquids.wad
\half-life\cstrike\de_piranesi.wad

//////Skies
gfx/env/cxup.tga
gfx/env/cxdn.tga
gfx/env/cxlf.tga
gfx/env/cxrt.tga
gfx/env/cxft.tga
gfx/env/cxbk.tga

//////Sounds
sound/ambience/sparrow.wav
sound/ambience/3dmeagle.wav
sound/ambience/crow.wav
sound/ambience/birds6.wav
sound/ambience/birds4.wav
sound/ambience/birds9.wav
sound/ambience/cow.wav
sound/ambience/sheep.wav
sound/ambience/dog1.wav
sound/ambience/dog2.wav
sound/ambience/dog3.wav
sound/ambience/dog4.wav
sound/ambience/dog5.wav
sound/ambience/waterrun.wav
sound/ambience/car1.wav
sound/ambience/car2.wav
sound/ambience/tankidle2.wav

//////Graphics

//////Sprites
sprites/glow01.spr

//////Models

//////Misc
// Overview : yes
// Waypoints: no
