Arabia2 - Bomb/Defuse
by Dirk "Cortex" Jurick (Dackyy@gmx.de)
http://pc.exit.de/cortex

Counter-Terrorists: Prevent Terrorists
from bombing the tank or the starting point
of the Counter-Terrorists.
Team members must defuse any bombs 
that threaten targeted areas.

Terrorists: The Terrorist carrying the
C4 must destroy either the tank or the
starting point of the Counter-Terrorists.


Other Notes: There are 2 bomb
targets in the mission. 

(Press FIRE to continue)