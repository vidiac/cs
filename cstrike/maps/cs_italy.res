////////////////////////////////////////////////////////////////
// cs_italy resources file
//
// � Chapo 2004 - Infos @ http://www.17buddies.net
//
// Can you play all of our maps?? Sure not!!
//

//////Wads
\sierra\half-life\cstrike\cstrike.wad
\sierra\half-life\valve\halflife.wad
\sierra\half-life\cstrike\decals.wad
\pics\itsitaly.wad

//////Skies
gfx/env/greenup.tga
gfx/env/greendn.tga
gfx/env/greenlf.tga
gfx/env/greenrt.tga
gfx/env/greenft.tga
gfx/env/greenbk.tga

//////Sounds
sound/ambience/guit1.wav
sound/ambience/opera.wav
sound/misc/killchicken.wav
sound/misc/sheep.wav

//////Graphics

//////Sprites
sprites/flare1.spr

//////Models
models/scientist.mdl
models/winebottle.mdl
models/chick.mdl
models/feather.mdl

//////Misc
// Overview : yes
// Waypoints: no
