===========================================================================
RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS RUS
===========================================================================
(����� �������� ������������ ������� ���� Courier New)	  english see below

1. ����� ����������
   ~~~~~~~~~~~~~~~~
   �������� ...... De_Priest (���������, ����, ��������� ������. ��������: �����)
   ���� .......... 25 ��� 2004�.
   ����� ......... ������� �������� a.k.a. Dmitrich!
   E-mail ........ cdi@sbor.net (�� �������! ������ ���� :)
   ����� ......... dmitrich.by.ru, cs-mapper.by.ru
   �������� ...... ���� ��� �������� ����� dm_aztec_maso, ���������� � ��� �����
   ������ ����� .. de_asche, ka_100x100, de_miracle_beta1, dm_aztec_maso (1,2,3)
   ���. ���� ..... ���� ��������� ���� � ����� BuZZeR'� DE_INDUST, thanx BuZZeR :) 


2. �����, ��������� � ���� ������
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CSTRIKE>Maps
   de_priest.bsp
   de_priest.txt
   de_priest_readme.txt (*���� ����*)
   de_priest.res
CSTRIKE>Gfx>Env
   ghettoup.tga (�������� ����)
   ghettodn.tga
   ghettolf.tga
   ghettort.tga
   ghettoft.tga
   ghettobk.tga
CSTRIKE>Sound>Ambience
   100jump.wav (���� ������ � ���������)
CSTRIKE>Sprites
   fire2.spr (������ ��������� �������)
CSTRIKE>overviews
   de_priest.bmp (�������� ��� ������ ����� ������)
   de_priest.txt (����� ��� ������ ��������)
CSTRIKE>PODBot/WPTDefault
   de_priest.pwf (���� ��� ���� PODBot)
   de_priest.pxp ("����", ����������� ������ �� ���� �����)


3. ���������� � �����
   ~~~~~~~~~~~~~~~~~~	
   ����. ������� ....... 32
   �������� ............ Valve Hammer Editor 3.5 (beta)
   ��.��������� ........ ZHLT Custom Build 1.7, Wally 1.55b, Photoshop 7.0
   ����� �������� ...... 2,5 ������ (������ ������ �� ��������� ����, ������ :)
   ��� �� .............. P3-533 MHz / 128 Mb (Windows XP Pro)
   ����� ������� ....... 1 ��� : 18 ���. 


4. ���������� ��� ��������
   ~~~~~~~~~~~~~~~~~~~~~~~
   ����. r_speeds ...... 700-720 wpoly
   ����. r_speeds ...... < 550 wpoly
   texdata ............. 2,30 MB (55%)
   models .............. 92/400 (23%)
   visibility matrix ... 48.9 megs
   hlcsg.exe -estimate -wadconfig de_priest "%mapname%" 
   hlbsp.exe -estimate "%mapname%" 
   hlvis.exe -low -estimate -full "%mapname%" 
   hlrad.exe -low -chart -estimate -extra -smooth 100 -dscale 1 -bounce 4 "%mapname%"


5. ��������� �����
   ~~~~~~~~~~~~~~~
   ���������� ��� ����� � ����� Half-Life/cstrike � ������������ � ���������� ����� � ������


6. ��������� �����
   ~~~~~~~~~~~~~~~
   ���� ��������� � ����������� ������ �� �-mail: cdi@sbor.net
   ���� ���, ���� �� ������ ������������� ��� ����� ����� �������, �������� � ������� (!) �������� ������� CS-�������� :)
   ���� �� ��������� �������� ������ �� ��������������� ���� ����� - ��������� �� ���� �� e-mail � ���������� ����� ������ (������ � ���� �� 50% :)
   �� ������ ��������������� �����, �� ������ � ����� ��������� �� "����������".
   ���� �� ���-���� (��-��-��) ������ ������� ������ ������ ����� - �������� ��� ������ �� ������ (����� ��. ��. 146 � � ��).
   ��. 146 �� "��������� ��������� � ������� ����". ���� ���� ������ �� ����, ����������� ������ �������� ���������� ������ �� ���� ��� � ������������ ���������.
   � ���������, �������: ��������������� ����� ������ � ���� ������ ��� ���������.

(c) Copyright 2004 ������� �������� a.k.a. Dmitrich!.
                                                     
===========================================================================
ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG ENG
===========================================================================
(best viewed with monospaced font like Courier New)
	
1. General information
   ~~~~~~~~~~~~~~~~~~~	
   Title ....... De_Priest
   Date ........ May 25, 2004
   Author ...... Dmitry Cherkashin a.k.a. Dmitrich!
   E-mail ...... cdi@sbor.net (don't spam! or I'll send you my swap file :)
   Sites ....... dmitrich.by.ru (my maps), cs-mapper.by.ru (mapping programs)
   Descr ....... if you like dm_aztec_maso, you'd like this map
   Other maps .. de_asche, ka_100x100, de_miracle_beta1, dm_aztec_maso (1,2,3)


2. Files came with this map
   ~~~~~~~~~~~~~~~~~~~~~~~~
CSTRIKE>Maps
   de_priest.bsp
   de_priest.txt
   de_priest_readme.txt (*this file*)
   de_priest.res
CSTRIKE>Gfx>Env
   ghettoup.tga (skybox pictures)
   ghettodn.tga
   ghettolf.tga
   ghettort.tga
   ghettoft.tga
   ghettobk.tga
CSTRIKE>Sound>Ambience
   100jump.wav (jumping sound)
CSTRIKE>Sprites
   fire2.spr (big flame sprite)
CSTRIKE>overviews
   de_priest.bmp (need for overview feature)
   de_priest.txt (need for overview feature too)
CSTRIKE>PODBot/WPTDefault
   de_priest.pwf (ways for PODBot)
   de_priest.pxp ("experience" collected by bots on this map)


3. Map information
   ~~~~~~~~~~~~~~~	
   Max # of players .... 32
   Editor used ......... Valve Hammer Editor 3.5 (beta)
   Other progs ......... ZHLT Custom Build 1.7, Wally 1.55b, Photoshop 7.0
   Build time .......... 2,5 months (in fact several days, I'm so lazy :)
   PC used ............. P3-533 MHz / 128 Mb (Windows XP Pro)
   Compilation time .... 1 hour : 18 mins. 


4. Information for mappers
   ~~~~~~~~~~~~~~~~~~~~~~~
   max ................. 700-720 wpoly
   avr. r_speeds ....... < 550 wpoly
   texdata ............. 2,30 MB (55%)
   models .............. 92/400 (23%)
   visibility matrix ... 48.9 megs
   hlcsg.exe -estimate -wadconfig de_priest "%mapname%" 
   hlbsp.exe -estimate "%mapname%" 
   hlvis.exe -low -estimate -full "%mapname%" 
   hlrad.exe -low -chart -estimate -extra -smooth 100 -dscale 1 -bounce 4 "%mapname%"


5. Installation
   ~~~~~~~~~~~~
   Unzip all files into your Half-Life/cstrike directory


6. Copyright-Permissions
   ~~~~~~~~~~~~~~~~~~~~~
   Authors MAY NOT use this level as a base to build additional levels. 
   You MUST NOT distribute this level UNLESS you INCLUDE THIS FILE WITH
   NO MODIFICATIONS!!! If you don't co-operate, then DON'T DISTRIBUTE
   IT IN ANY FORM!!

   This BSP may be distributed ONLY over the Internet and/or BBS systems.
   You are NOT authorized to put this BSP on any CD or distribute it in
   any way without my written permission.

(c) Copyright 2004 Dmitry Cherkashin a.k.a. Dmitrich!
	
--
EOF