[ru]
NEWSNOW_SNOW_TITLE	= \r[\yNewYear\r]\w Уровень снега
NEWSNOW_SNOW_1		= Выключено
NEWSNOW_SNOW_2		= Малый уровень
NEWSNOW_SNOW_3		= Средний уровень
NEWSNOW_SNOW_4		= Высокий уровень

NEWYEAR_MENU_TITLE	= \r[\yNewYear\r]\w Объекты на карте^nНаведите прицел на нужное место
NEWYEAR_MENU_ADD	= Добавить
NEWYEAR_MENU_MOVE	= Переместить \y(\w%L\y)
NEWYEAR_MENU_SEL	= \rВыбрано
NEWYEAR_MENU_NOSEL	= Не выбрано
NEWYEAR_MENU_DELETE	= Удалить
NEWYEAR_MENU_ROTATE	= Повернуть
NEWYEAR_MENU_SAVE	= \yСохранить ВСЕ объекты
NEWYEAR_MENU_DELALL	= \rУдалить ВСЕ объекты
NEWYEAR_MENU_TYPE	= \r[\y%s\r]\w - %L
NEWYEAR_DECOR = Декорация
NEWYEAR_PRESENT = Подарок

NEWYEAR_ADD_OK		= !n[!gNewYear!n]!n Объект !tдобавлен!n! Не забудьте сохранить!
NEWYEAR_MOVE_SELECT	= !n[!gNewYear!n]!n Объект !tвыбран!n! Передвиньте его в другое место.
NEWYEAR_MOVE_OK		= !n[!gNewYear!n]!n Объект успешно !tперемещен!n! Не забудьте сохранить!
NEWYEAR_DELETE_OK	= !n[!gNewYear!n]!n Объект успешно !tудален!n!
NEWYEAR_DELETE_ALL	= !n[!gNewYear!n]!n Все объекты успешно !tудалены!n! Не забудьте сохранить изменения!
NEWYEAR_SAVE_ALL	= !n[!gNewYear!n]!t Все изменения успешно сохранены!!g %d!n объектов на карте !g%s
NEWYEAR_ROTATE_OK	= !n[!gNewYear!n]!n Объект повернут на !g45гр

NEWYEAR_ROTATE_ERROR	= Нет объектов по прицелу!
NEWYEAR_DELETE_ERROR	= Нет объектов по прицелу!
NEWYEAR_MOVE_ERROR		= Нет объектов по прицелу!
NEWYEAR_MOVE_NOSEL		= Объект не выбран!
NEWYEAR_ADD_ERROR		= Нельзя поставить сюда объект!

NEWYEAR_BONUS_GIVE = !n[!gNewYear!n]!n Вы получили !g%L
NEWYEAR_BONUS_SECONDARY = %s
NEWYEAR_BONUS_PRIMARY = %s
NEWYEAR_BONUS_MONEY = %d$
NEWYEAR_BONUS_XP = %dXP
NEWYEAR_BONUS_ANEW = %d Очков /anew
NEWYEAR_BONUS_GOLDAK47 = Золотой AK47
NEWYEAR_BONUS_GOLDM4A1 = Золотой M4A1
NEWYEAR_BONUS_HP = %dHP
NEWYEAR_BONUS_AP = %dAP
NEWYEAR_BONUS_NULL = !tничего :D