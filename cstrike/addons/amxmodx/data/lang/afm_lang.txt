[ru]
MSG_POKE = Эй !g%s!n! Почему ты все еще AFK? Кто-то убил этого дурака
MSG_PROD = Эй !g%s!n! Почему ты все еще AFK? Кто-то убил этого дурака
MSG_BOMB = !g%s !nвыбросил бомбу за AFK более !t%i секунд
MSG_HURT = !g%s !nначал терять HP за AFK более !t%i секунд
MSG_SLAY = !g%s !nбыл убит за AFK более !t%i секунд
MSG_SPEC = !g%s !nбыл перенесен в спектаторы за AFK более !t%i секунд
MSG_KICK = !g%s !nбыл кикнут за AFK более !t%i секунд
DAS_BOOT = AFK более %i секунд

LOG_POKE = %s [ %s | %s ] poked for being AFK %i seconds.
LOG_PROD = %s [ %s | %s ] prodded for being AFK %i seconds.
LOG_NAME = Changed name of %s to %s for being AFK.
LOG_HURT = %s [ %s | %s ] is being hurt for being AFK for over %i seconds.
LOG_BOMB = %s [ %s | %s ] dropped bomb for being AFK for over %i seconds.
LOG_SLAY = Slayed %s [ %s | %s ] for being AFK over %i seconds.
LOG_SPEC = Transferred %s [ %s | %s ] to spectate for being AFK for over %i seconds.
LOG_KICK = Kicked %s [ %s | %s ] for being AFK for over %i seconds.

NOACCESS = !tКоманда недоступна

[en]
MSG_POKE = Hey %s! Why you all so afk? Someone kill this fool.
MSG_PROD = Hey %s! Why you all so afk? Someone kill this fool.
MSG_BOMB = %s was forced to give up bomb privileges for being AFK longer than %i seconds.
MSG_HURT = %s begins losing health for being AFK longer than %i seconds.
MSG_SLAY = %s was slain for being AFK longer than %i seconds.
MSG_SPEC = %s was moved to spectator for being AFK longer than %i seconds.
MSG_KICK = %s was kicked for being AFK longer than %i seconds.
DAS_BOOT = You were kicked for being AFK longer than %i seconds.

LOG_POKE = %s [ %s | %s ] poked for being AFK %i seconds.
LOG_PROD = %s [ %s | %s ] prodded for being AFK %i seconds.
LOG_NAME = Changed name of %s to %s for being AFK.
LOG_HURT = %s [ %s | %s ] is being hurt for being AFK for over %i seconds.
LOG_BOMB = %s [ %s | %s ] dropped bomb for being AFK for over %i seconds.
LOG_SLAY = Slayed %s [ %s | %s ] for being AFK over %i seconds.
LOG_SPEC = Transferred %s [ %s | %s ] to spectate for being AFK for over %i seconds.
LOG_KICK = Kicked %s [ %s | %s ] for being AFK for over %i seconds.

NOACCESS = Command unavailable.