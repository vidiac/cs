[ru]
LVL_1 = Курсант
LVL_2 = Рядовой
LVL_3 = Ефрейтор
LVL_4 = Мл.сержант
LVL_5 = Сержант
LVL_6 = Ст.сержант
LVL_7 = Старшина
LVL_8 = Прапорщик
LVL_9 = Ст.прапорщик
LVL_10 = Мл.лейтенант
LVL_11 = Лейтенант
LVL_12 = Ст.лейтенант
LVL_13 = Капитан
LVL_14 = Майор
LVL_15 = Подполковник
LVL_16 = Полковник
LVL_17 = Генерал - Майор
LVL_18 = Генерал - Лейтенант
LVL_19 = Генерал - Полковник
LVL_20 = Генерал - Армии
LVL_21 = Маршал РФ

AR_TAG = !n[!gАрмия!n]
AR_TAG_MENU = \r[\yАрмия\r]\w
AR_TAG_CON = [Армия]

AR_ON = Вкл.
AR_OFF = Выкл.

AR_NOPLAYERS = Нельзя ставить бомбу, если один на серве!
AR_NEWLEVEL_ALL = Боец !t%s !nполучил новое звание - !g%L!n. Поздравляем!
AR_NEWLEVEL_ID = Вы получили новое звание - !g%L!n. Поздравляем!
AR_NICK = Ник
AR_ZVANIE = Звание
AR_PLAYER_XP = Опыт
AR_NEXT_LVL = До след.звания : %d опыта
AR_PLAYER_XP_MAX = Достигнут максимум развития

AR_INFORMER0 = Ник : %s^n
AR_INFORMER1 = Звание : %L^n
AR_INFORMER2 = Опыт : [%d/%d]

AR_BONUS_DESABLE = !tБонусы отключены!

AR_ANEW_DISABLE = !tФункция отключена
AR_ANEW_BLOCKED = !tЗапрещено на данной карте!
AR_ANEW_ROUND = !tДоступно с !g%d-ого !tраунда!
AR_ANEW_ALIVE = !tДоступно только для живых!
AR_ANEW_BUYTIME = %d секунд прошло. Нельзя ничего купить!
AR_ANEW_NOT = У вас не хватает бонусных очков! (%d)
AR_ANEW_MENU = Что хотите взять?
AR_ANEW_AWP = AWP \y+комплект \d[\%s%d\d]
AR_ANEW_AK47 = AK47 \y+комплект \d[\%s%d\d]
AR_ANEW_M4A1 = M4A1 \y+комплект \d[\%s%d\d]
AR_ANEW_MONEY = %d\r$ \d[\%s%d\d]
AR_ANEW_HP = %d\rHP \d[\%s%d\d]
AR_ANEW_GREN = MegaGrenade \r(\y%d%% \wурона\r) \d[\%s%d\d]
AR_ANEW_DEAGLE = MegaDeagle \r(\y%d%% \wурона\r) \d[\%s%d\d]
AR_ANEW_RENDER = Невидимость \r(\y%d%% \wвидимости\r) \d[\%s%d\d]
AR_ANEW_GIVE = Вы взяли!t
AR_ANEW_GIVE_GREN = MegaGrenade(%d%%)
AR_ANEW_GIVE_DEAGLE = MegaDeagle(%d%%)
AR_ANEW_GIVE_RENDER = Невидимость(%d%%)
AR_ANEW_HUD = У вас есть %d очков бонуса^nНапишите в чат /%s
AR_ANEW_FRAGS = Вы убили!g %d !nигроков подряд.!t +%d !nбонус.
AR_ANEW_HS = Вы убили!g %d !nигроков подряд в голову.!t +%d !nбонус.
AR_ANEW_KNIFE = Вы убили!g %d !nигроков подряд с ножа.!t +%d !nбонус.
AR_ANEW_HE = Вы убили!g %d !nигроков подряд с гранаты.!t +%d !nбонус.

AR_DED_ID = Вы украли у !g%s!t
AR_DED_PLAYER = украл у Вас!t
AR_DED_SLAP_ID = Вы пнули !g%s !nна !t%d!nНР
AR_DED_SLAP_PLAYER = !g%s !nпнул Вас на !t%d!nНР
AR_DED_FAIL = !g%s !tоказался старше Вас по званию. Вы ничего не украли.
AR_DED_FAIL_WEAPON = !tНевозможно украсть нож!
AR_DED_LOCK = Попробовать еще можно через !t%d !nраунда(ов)
AR_DED_FIRSTROUND = !tДоступно с !g%d-ого !tраунда!

STEAM_ID_LAN = Ваша версия CS кривая или устарелая. Статистика званий не будет работать.
DOWNLOAD_CS = Скачать нормальную версию можно тут: http://cs.malnet.ru
AR_LOAD_FAIL = !tНе удалось загрузить звания...

AR_HUD_DISABLE = Hud сообщение с текущим званием !tВыключено!n!
AR_HUD_ENABLE = Hud сообщение с текущим званием !tВключено!n!

AR_SQLDATA = SELECT frags, deaths, headshots, def, exp, addxp, anew FROM `%s` WHERE authid = '%s';

AR_MOTD_META = <META http-equiv=Content-Type content='text/html;charset=UTF-8'>
AR_MOTD_STYLE = <style>table{width:100%%;line-height:160%%;}</style>%s
AR_MOTD_TOP = <p>%s</p><table cellspacing=0 class=q>
AR_MOTD_CLOSE = </table>
AR_MOTD_ASTATS = Статистика
AR_MOTD_AINFO = Настройки
AR_MOTD_FUNCTION = Функция
AR_MOTD_STATUS = Статус
AR_MOTD_XP_VALUE = Количество опыта за одно убийство
AR_MOTD_XP_C4DEF = Koличecтвo oпытa зa взpыв или paзминиpoвaниe бoмбы
AR_MOTD_XP_HEADSHOT = B двa paзa бoльшe oпытa зa убийствo в гoлoву
AR_MOTD_BONUS = Бoнуcы зa звaния
AR_MOTD_ANEW = Бoнуcнoe мeню (/%s)
AR_MOTD_FIRSTROUND = Дo кaкoгo paундa нe будут выдaвaтьcя бoнуcы
AR_MOTD_FIRSTROUND_ON = <td class=iN>Дo <b>%d</b>
AR_MOTD_FIRSTROUND_OFF = <td class=iY>Выдaютcя вceгдa
AR_MOTD_DED_INFO = Дeдoвщинa. Зaжaть кнoпку "E" и нaвecти пpицeл нa coкoмaндникa
AR_MOTD_ABONUS = Бoнуcы в нaчaле рaундa
AR_MOTD_YOUR_LVL = Baшe звaние
AR_MOTD_NEXT_LVL = Cлeд.звaние
AR_MOTD_BONUS_HE = Гpaнaтa
AR_MOTD_BONUS_FLASH = Cлeпa
AR_MOTD_BONUS_SMOKE = Дымкa
AR_MOTD_BONUS_ARMOR = <b>%d</b> AP
AR_MOTD_BONUS_DEFUSE = Defuse
AR_MOTD_BONUS_NV = Нoчнoe видeниe
AR_MOTD_BONUS_HEALTH = +<b>%d</b> HP
AR_MOTD_BONUS_FLAGS = Флaг: <b>%s</b>
AR_MOTD_BONUS_DAMAGE = Бoльше уpoн нa <b>%d%%</b>
AR_MOTD_ANEW_H1 = Бoнуcы /%s
AR_MOTD_ANEW_JOBS = Мини задания
AR_MOTD_ANEW_NEWLEVEL = <b>%d</b> за нoвый уpoвeнь
AR_MOTD_ANEW_FRAGS = Зa <b>%d</b> фpaгов - <b>%d</b>Бонуса
AR_MOTD_ANEW_HS = Зa <b>%d</b> HeadShots - <b>%d</b>Бонуса
AR_MOTD_ANEW_KNIFE = Зa <b>%d</b> нoжей - <b>%d</b>Бонуса
AR_MOTD_ANEW_HE = Зa <b>%d</b> гpaнaту - <b>%d</b>Бонуса
AR_MOTD_ANEW_INFO = <font size=2>Koличecтвo убийcтв пocлe cмepти oбнуляeтcя</font>
AR_MOTD_ALIST = Звания
AR_ARMYMENU_PLAYERS = Выберите игрока \w(\yУровень\w) (\yРеал.XP+Доп.XP\w) (\yanew\w)
AR_ARMYMENU_ITEM = %s \w(\y%d\w) (\y%d+%d\w) (\y%d\w)
AR_ARMYMENU_EDITXP = Добавить опыт
AR_ARMYMENU_EDITANEW = Добавить бонусные очки \y/anew
AR_EDITXP_EDIT = Редактировать опыт:
AR_EDITXP_ITEM = %s \r%L \y%d+%d=%d\wXP
AR_EDITXP_EDIT2 = \y0 \w- установить опыт по умолчанию
AR_EDITXP_ADD = Добавить \y%d
AR_EDITXP_ROB = Отнять \r%d
AR_EDITXP_MANUALLY = Ввести вручную
AR_EDITXP_STEP = Шаг %s
AR_EDITXP_MESSAGEMODE = Сколько опыта добавить? ("-" отнять опыт)
AR_EDITXP_STEAM = Введите в консоли: !t%s
AR_EDITXP_CHATCLEAR = Установил !g%s !nопыт по умолчанию
AR_EDITXP_CHATCLEAR_ALREADY = У !g%s !nопыт и так по умолчанию!
AR_EDITXP_CHATADD = %L !g%s !t%d!n опыта
AR_EDITXP_CHATADMIN = !g%s !n- !t%L !n(!g%d!n)
AR_EDITXP_CHATPLAYER = Админ !g%s !n%L Вам !t%d !nопыта
AR_EDITXP_CHATPLAYER_DEFAULT = Админ !g%s !nУстановил Вам опыт по умолчанию
AR_EDITXP_AUTHID_ERROR = !tУ игрока !g%s !tне сгенерировался AuthID. Добавление опыта невозможно!
AR_EDITANEW_EDIT = Редактировать бонусные очки \y/anew\w:
AR_EDITANEW_ITEM = %s \w(\y%d\w)
AR_EDITANEW_AUTHID_ERROR = !tУ игрока !g%s !tне сгенерировался AuthID. Бонусы добавлены, но не сохранены!
AR_EDITANEW_CHATCLEAR = Обнулил бонусы !g%s
AR_EDITANEW_CHATPLAYER_DEFAULT = Админ !g%s !nобнулил Вам бонусы
AR_EDITANEW_CHATADD = %L !g%s !t%d!n бонус
AR_EDITANEW_CHATPLAYER = Админ !g%s !n%L Вам !t%d !nбонус
AR_EDITANEW_CHATADMIN = !g%s !n- !g%d!nбонус
AR_ADD = Добавил
AR_ROB = Отнял

AR_MENU_NAME = \wМеню \rArmy Ranks Ultimate
AR_MENU_ANEW = Бонусы за новый уровень
AR_MENU_ASTATS = Статистика игроков онлайн \y/%s
AR_MENU_AINFO = Настройки плагина \y/%s
AR_MENU_ABONUS = Бонусы \y/%s
AR_MENU_ALIST = Звания \y/%s
AR_MENU_AENABLE = \r%L \wHud сообщение \y/%s
AR_MENU_ATOP = Веб статистика \y/%s
AR_MENU_EXITLANG = Выход
AR_MENU_BACKLANG = Назад
AR_MENU_NEXTLANG = Вперед

AR_VAMPIRE_HUD = +%dHP

AR_BETS_TAG = !n[!gСтавки!n]
AR_BETS_TAGMENU = \r[\yСтавки\r]\w
AR_BETS_TITLE = Сделать ставку?^nОсталось \r%d\w секунд
AR_BETS_NOTBETS = \dНет ставок
AR_BETS_MENU_BET = Ставка: 
AR_BETS_MENU_MONEY = \r%d\w$
AR_BETS_MENU_XP = \r%d \wОпыта
AR_BETS_ALREADY = \yВы уже сделали ставку! Изменить ее нельзя!
AR_BETS_PLAYER_T = %s \r[\yT\r]
AR_BETS_PLAYER_CT = %s \r[\yCT\r]
AR_BETS_WIN = \r(\yВыигрыш: %s\r)
AR_BETS_MONEY = Деньги %s
AR_BETS_XP = Опыт %s
AR_BETS_CHAT_NOMONEY = !tУ Вас недостаточно денег!
AR_BETS_CHAT_NOXP = !tУ Вас недостаточно опыта!
AR_BETS_CHAT_MONEY = !g%d!n$
AR_BETS_CHAT_XP = !g%d !nОпыта
AR_BETS_CHAT_BET = !g%s !nпоставил на !t%s!n: %s
AR_BETS_CHAT_WINNER = !g%s !tПобедил! !nВы получаете: %s
AR_BETS_CHAT_LOOSER = !g%s !tПроиграл...
AR_BETS_CHAT_NOTWINNER = !tНикто не выиграл. Ставки возвращены.
AR_BETS_INFO = Напишите !g/bets!n чтобы сделать ставку
AR_BETS_ALIVE = !tДоступно только для мертвых!
AR_BETS_NOT = !tДоступно если остались двое(1х1)
AR_BETS_TIME = !tВремя на ставку прошло...