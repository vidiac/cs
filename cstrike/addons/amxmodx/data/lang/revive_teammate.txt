[en]
MENU_NAME = \r[\yRevival menu\r]
DEAD_DENYED = You can't revive your teammates when your dead.
MONEY_SHORT = You don't have enough money (%d$) to revive a teammate.
HAS_REVIVED = '%s' has revived his teammate '%s'
ALL_TMMS_ALIVE = All of your teammates are alive.

[lt]
MENU_NAME = \r[\yPrikelimo meniu\r]
DEAD_DENYED = Tu negali prikelti savo komandos nariu, kai esi negyvas.
MONEY_SHORT = Tu neturi pakankamai pinigu (%d$), kad galetum prikelti komandos nari.
HAS_REVIVED = '%s' prikele savo komandos nari '%s'
ALL_TMMS_ALIVE = Visi tavo komandos nariai yra gyvi.

[de]
MENU_NAME = \r[\yWiederbelebungs Menue\r]
DEAD_DENYED = Du kannst keine Teamkameraden wiederbeleben wenn du tot bist.
MONEY_SHORT = Du hast nicht genuegend Geld (%d$) um einen Teamkameraden zu wiederbeleben.
HAS_REVIVED = '%s' hat seinen Teamkameraden '%s' wiederbelebt
ALL_TMMS_ALIVE = All deine Teamkameraden sind noch am Leben.

[bg]
MENU_NAME = \r[\ySujivqvashto meniu\r]
DEAD_DENYED = Ne moje da sujivqvate suotbornicite si kogato ste murtvi.
MONEY_SHORT = Nqmate dostatachno pari (%d$) za da sajivite suotbornika si.
HAS_REVIVED = '%s' sujivi svoq suotbornik '%s'
ALL_TMMS_ALIVE = Vsichki tvoi suotbornici sa jivi.

[ro]
MENU_NAME = \r[\yMeniu de reinviere\r]
DEAD_DENYED = Poti sa iti reinvii coechipierii cand esti mort.
MONEY_SHORT = Nu ai destui bani ca sa iti reinvii un coechipier.
HAS_REVIVED = '%s' l-a reinviat pe '%s'
ALL_TMMS_ALIVE = Toti coechipierii tai sunt in viata.

[es]
MENU_NAME = \r[\yMenu de Revivir\r]
DEAD_DENYED = No puedes revivir a tus companieros cuando estas muerto.
MONEY_SHORT = No tienes suficiente dinero (%d$) para revivir a un companiero.
HAS_REVIVED = '%s' a revivido a su companiero '%s'
ALL_TMMS_ALIVE = Todos tus companieros de equipo estan vivos.

[pl]
MENU_NAME = \r[\yMenu Wskrzeszania\r]
DEAD_DENYED = Nie mozesz wskrzesic kolegi kiedy jestes martwy.
MONEY_SHORT = Nie masz wystarczajacej ilosci pieniedzy (%d$) zeby wskrzesic kolege.
HAS_REVIVED = '%s' wskrzesil kolege '%s'
ALL_TMMS_ALIVE = Wszyscy twoi koledzy sa zywi.

[ru]
MENU_NAME = \r[\yМеню оживления\r]
DEAD_DENYED = Вы не можете оживить товарища по команде, т.к. вы мертвы.
MONEY_SHORT = У Вас недостаточно денег (%d$), чтобы оживить товарища по команде.
HAS_REVIVED = '%s' оживил товарища по команде '%s'
ALL_TMMS_ALIVE = Все Ваши товарищи по команде живы.

[nl]
MENU_NAME = \r[\yTerug Tot Leven Wekken\r]
DEAD_DENYED = Je kan je teamleden niet terug tot leven wekken wanneer je dood bent.
MONEY_SHORT = Je hebt niet genoeg geld (%d$) om een teamlid terug tot leven te wekken.
HAS_REVIVED = '%s' heeft zijn teamlid '%s' terug tot leven gewekt
ALL_TMMS_ALIVE = All jouw teamleden zijn levend.

[tr]
MENU_NAME = \r[\yDiriltme menusu\r]
DEAD_DENYED = Olu iken takim arkadaslarinizi diriltemezsiniz.
MONEY_SHORT = Yeterli paraniz yok, bir takim arkadasi diriltmek (%d$).
HAS_REVIVED = '%s' takim arkadasini diriltti '%s'
ALL_TMMS_ALIVE = Tum takim arkadaslariniz hayatta.
