[ru]
VIP_TAG = !n[!gVIP!n]
VIP_NOTACCESS = !tНедостаточно прав!
VIP_MENU_TITLE = \r[\yVIP\r]\w Меню \R%d/%d

VIP_BLOCKROUNDS = Доступно через %d раунда
VIP_BLOCKSECONDS = Доступно через %d секунд
VIP_ALIVE = Доступно только для живых!

VIP_FIRSTROUND = !tДоступно только с !g%d раунда!
VIP_BUYTIME = Прошло %d секунд. Вы не можете использовать VIP Menu!
VIP_DEAD_ONLY = Можно использовать, только когда мертв
VIP_RESPAWN_ALLDEAD = Нельзя воскреснуть, когда вся команда мертва!
VIP_GOLD_MONEY = Недостаточно денег!
VIP_RESTRIC_MAP = Запрещено брать на этой карте!

VIP_MENU_MONEY = \y%s\w$
VIP_MENU_RENDER = \y%s%% \wневидимости
VIP_MENU_DAMAGEATTAKER = Увеличение урона на \y%s%%
VIP_MENU_DAMAGEVICTIM = Уменьшение полученного урона на \y%s%%
VIP_MENU_HEALTH = \y%s\wHP
VIP_MENU_SPEED = Ускорение на \y%s%%
VIP_MENU_RESPAWN = Возрождение
VIP_MENU_GOLDAK47 = Gold AK47 \r(\y%s$\r)
VIP_MENU_GOLDM4A1 = Gold M4A1 \r(\y%s$\r)

VIP_MENU_WEAPON1 = AWP/Deagle

VIP_MENU_EXEC1 = Меню урона
VIP_MENU_EXEC2 = Усиленные оружия

VIP_MENU_CHAT = Вы взяли:

VIP_MORE_CONNECT = VIP !g%s!n присоединился к игре!
VIP_MORE_CONNECTADMIN = ADMIN !g%s!n присоединился к игре!
VIP_ONLINE_MSG = VIP Online:
VIP_ONLINE_NONE = !tнету

VIP_EXPIRED = Випка истекает через \r[\y%d\r]\w %s
VIP_EXPIRED_ON = \rВаша випка истекла!