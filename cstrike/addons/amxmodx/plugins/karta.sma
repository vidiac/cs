#include <amxmodx>

#define PLUGIN "block_changelevel"
#define VERSION "0.2"
#define AUTHOR "MiXa"

#define GL_ADMIN_FLAG ADMIN_IMMUNITY // ���� ������, ������� ������ ������ ����� � ����� �����

new round_number, q_block

public plugin_init()
{
    register_plugin(PLUGIN, VERSION, AUTHOR)

    q_block = register_cvar("amx_block_changelevel", "10") // � ������ ������ ��������� ����� �����

    register_clcmd("amx_map",         "block_changelevel")
    register_clcmd("amx_votemap",     "block_changelevel")
    register_clcmd("changelevel",     "block_changelevel")
    register_clcmd("amx_mapmenu",     "block_changelevel")
    register_clcmd("amx_votemapmenu", "block_changelevel")

    register_logevent("event_round_start", 2, "1=Round_Start")
    register_event("TextMsg", "event_round_restart", "a", "2=#Game_will_restart_in","2=#Game_Commencing");
}

public event_round_start ()
    round_number++

public event_round_restart()
    round_number = 0

public block_changelevel(id)
{
    if (!(get_user_flags ( id ) & GL_ADMIN_FLAG ))
    {
        if ( round_number <= get_pcvar_num(q_block)-1 )
        {
            client_print(id,print_chat,"����� ����� ��������� � %d ������, ������ %d �����", get_pcvar_num(q_block), round_number);
            client_print(id,print_console,"����� ����� ��������� � %d ������^n������ %d �����", get_pcvar_num(q_block), round_number);
            return PLUGIN_HANDLED
        }
    }
    return PLUGIN_CONTINUE
}
