/*
	Как дать Випу дополнительный опыт за убийство? Смотрите ниже ;)
	
	P.S. такой плагин будет работать только совместно с csstats_mysql.amxx, так как
	дополнительный опыт писаться в Базу Данных MySQL. 
	Без csstats_mysql.amxx, players.ini загадится игроками и сервер начнет лагать и может упасть.
*/

#include <amxmodx>
#include <amxmisc>
#include <army_ranks_ultimate>

#define VIP_ACCESS_LEVEL ADMIN_LEVEL_H // Flag "t"

#define PLUGIN	"AR Double"
#define VERSION	"14.12.02"
#define AUTHOR	"SKAJIbnEJIb"

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	register_event("DeathMsg", "EventDeath", "a", "1>0")
}

public EventDeath() 
{
	new victim = read_data(2) // жертва
	new killer = read_data(1) // убийца

	// разные проверки+проверка на VIP доступ
	if (killer != victim && !is_user_bot(killer) && get_user_flags(killer) & VIP_ACCESS_LEVEL && get_user_team(killer) != get_user_team(victim)) 
	{
		new headshot = read_data(3) // в голову убил или нет
		if (headshot)
			ar_set_user_addxp(killer, 2) // если в голову, прибавляем к убийству еще 2 опыта
		else
			ar_set_user_addxp(killer, 1) // если просто убил, прибавим еще 1 опыт
	}
}