﻿#include <amxmodx>
#include <amxmisc>
#include <colorchat>

#define COLORS_DATA_SIZE 8
#define MAX_PLAYERS 32

#define TIME_TO_SHOW 5 //Через сколько секунд показать меню.

//#define isVIP(%0) (get_user_flags(%0) & ADMIN_BAN)

const KEYSMENU = MENU_KEY_0|MENU_KEY_1|MENU_KEY_2|MENU_KEY_3|MENU_KEY_4|MENU_KEY_5|MENU_KEY_6|MENU_KEY_7|MENU_KEY_8|MENU_KEY_9;

new g_szMenuItem[][] =
{
	"Без цвета", 	//0
	"Голубой",   	//1
	"Синий",     	//2
	"Жёлтый",    	//3
	"Зелёный",   	//4
	"Красный",   	//5
	"Розовый",   	//6
	"Белый"      	//7
};

new g_eEnable[MAX_PLAYERS + 1][COLORS_DATA_SIZE], g_iAlpha[MAX_PLAYERS + 1], g_iPrecent[MAX_PLAYERS + 1];
new g_iRed[MAX_PLAYERS + 1], g_iGreen[MAX_PLAYERS + 1], g_iBlue[MAX_PLAYERS + 1], g_iSayText;

const ACTIVE = 1;
const NOT_ACTIVE = 0;

public plugin_init()
{
	#define VERSION "1.2"
	register_plugin( "COLORS MENU", VERSION, "DUKKHAZ0R" );
	register_cvar("colors_version", VERSION, FCVAR_SERVER | FCVAR_SPONLY);
	
	register_event( "DeathMsg", "on_death", "a", "1>0" );
	
	register_menu( "menu_title", KEYSMENU, "menu_handler" );
	register_clcmd( "say", "hook_say" );
	register_clcmd( "say_team", "hook_say" );
	
	register_clcmd( "set_precent", "precent_clcmd" );
	
	g_iSayText = get_user_msgid("SayText");
}

public client_putinserver(id)
{
	//if(!isVIP(id)) return;
	
	g_eEnable[id][0] = ACTIVE;
	g_iPrecent[id] = 50;
	
	set_task( TIME_TO_SHOW.0, "show_colorsmenu", id );
}
		
public hook_say(id)
{
	static szMessage[192];
	read_args( szMessage, charsmax(szMessage) );
	remove_quotes( szMessage );
	
	if( szMessage[0] != '/' ) return;
	
	new gszColorsCmd[][] = { "/colors", "/color", ".color", ".colors" };
	
	for( new i; i < sizeof gszColorsCmd; i++ )
	{
		if( equal( szMessage, gszColorsCmd[i] ) )
			show_colorsmenu(id);
	}
}
	
public show_colorsmenu(id)
{
	//if(!isVIP(id))
	//{
	//	ChatColor( id, "^1[^4COLOR^1] Меню доступно только ^3VIP ^1игрокам" );
	//	return PLUGIN_HANDLED;
	//}
	static iLen, szMenu[512];
	
	iLen = formatex( szMenu, charsmax(szMenu), "\yЗатемнение при фраге:^n^n" );
	
	for( new i; i < sizeof g_szMenuItem; i++ )
		iLen += formatex( szMenu[iLen], charsmax(szMenu) - iLen, "\r%d.%s%s^n", i+1, g_eEnable[id][i] ? "\y" : "\w", g_szMenuItem[i] );
	
	iLen += formatex( szMenu[iLen], charsmax(szMenu) - iLen, "^n\r9.\wПрозрачность: \d%d%",g_iPrecent[id] );
	
	iLen += formatex( szMenu[iLen], charsmax(szMenu) - iLen, "^n^n\r0.\yВыход" );
	
	show_menu( id, KEYSMENU, szMenu, -1, "menu_title" );
	return PLUGIN_HANDLED;
}

public menu_handler(id, item)
{
	if( item == 9 )
	{
		return PLUGIN_HANDLED;
	}
	if( item == 8 )
	{
		client_cmd( id, "messagemode ^"set_precent ^"" );
		return PLUGIN_HANDLED;
	}
	
	arrayset( g_eEnable[id], NOT_ACTIVE, COLORS_DATA_SIZE );
	g_eEnable[id][item] = ACTIVE;
	
	item ? ChatColor( id, "^4* ^1Выбран ^3%s ^1цвет с прозрачностью^3 %d%", g_szMenuItem[item], g_iPrecent[id] ) : ChatColor( id, "^4* ^3затемнение при фраге - ^4выключено!" );
	
	switch(item)
	{
		case 0: {} //empty key
		case 1:
		{
			// 0 200 255
			g_iRed[id] = 0;
			g_iGreen[id] = 200;
			g_iBlue[id] = 255;
		}
		case 2:
		{
			// 0 0 255
			g_iRed[id] = 0;
			g_iGreen[id] = 0;
			g_iBlue[id] = 255;
		}
		case 3:
		{
			// 255 255 0
			g_iRed[id] = 255;
			g_iGreen[id] = 255;
			g_iBlue[id] = 0;
		}
		case 4:
		{
			// 0 255 0
			g_iRed[id] = 0;
			g_iGreen[id] = 255;
			g_iBlue[id] = 0;
		}
		case 5:
		{
			// 255 0 0
			g_iRed[id] = 255;
			g_iGreen[id] = 0;
			g_iBlue[id] = 0;
		}
		case 6:
		{
			// 255 0 255
			g_iRed[id] = 255;
			g_iGreen[id] = 0;
			g_iBlue[id] = 255;
		}
		case 7:
		{
			g_iRed[id] = 255;
			g_iGreen[id] = 255;
			g_iBlue[id] = 255;
		}
		case 8:
		{
			client_cmd( id, "messagemode ^"set_precent ^"" );
		}
	}
	return PLUGIN_HANDLED;
}

public precent_clcmd(id)
{
	new i_Data[6];
	read_argv( 1, i_Data, 5 );
	new iData = str_to_num(i_Data);
	
	if( iData < 0 || iData > 100 )
	{
		ChatColor( id, "^4* - ^1неверный формат числа!" );
		return;
	}
	
	g_iPrecent[id] = iData;
	ChatColor( id, "^4* ^1- затемнение при фраге теперь ^3%d%", g_iPrecent[id] );
}

public on_death()
{
	static nKiller;
	nKiller = read_data(1);

	if( !is_user_alive(nKiller) || !is_user_connected(nKiller) )
		return
	
	g_iAlpha[nKiller] = 255 - ( 2.55 * g_iPrecent[nKiller] );
	
	if( !g_eEnable[nKiller][0] /* && isVIP(nKiller) */ )
	{
		message_begin( MSG_ONE, get_user_msgid("ScreenFade"), {0,0,0}, nKiller );
		write_short( 1<<10 );
		write_short( 1<<10 );
		write_short( 0x0000 );
		write_byte( g_iRed[nKiller] );
		write_byte( g_iGreen[nKiller] );
		write_byte( g_iBlue[nKiller] );
		write_byte( floatround(g_iAlpha[nKiller]) );
		message_end();
	}
}

stock ChatColor(const id, const input[], any:...) 
{
	new count = 1, players[32]
	static msg[191]
	vformat(msg, 190, input, 3)
	replace_all(msg, 190, "!g", "^4") 
	replace_all(msg, 190, "!y", "^1") 
	replace_all(msg, 190, "!t", "^3") 
	if (id) players[0] = id; else get_players(players, count, "ch")
	{
		for (new i = 0; i < count; i++)
		{
			if (is_user_connected(players[i])) 
			{
				message_begin(MSG_ONE_UNRELIABLE, g_iSayText, _, players[i])
				write_byte(players[i])
				write_string(msg)
				message_end()
			}
		}
	}
}