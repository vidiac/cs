/*	Kills Counter by Safety1st
*	  ���������� ������ � HUD'� ����� ��� ������� � ������� ������.
*
*	���������:
*	  �������� ���������� ������ 9 �������, ������� ��� DM-�������� ������ �� �������.
*	  ���-����� �� ���������.
*
*	��������� ����������:
*	  13.12.2012
*
*	�������������:
*	  ������ ������������� ������� Scarzzurs �� ����
*/

#include <amxmodx>

#define PLUGIN "Kills Counter"
#define VERSION "1.0"
#define AUTHOR "Safety1st"

#define MAX_PLAYERS 32

new gMsgStatusIcon
new giCurrentKills[MAX_PLAYERS + 1]

public plugin_init() {
	register_plugin( PLUGIN, VERSION, AUTHOR )
	gMsgStatusIcon = get_user_msgid( "StatusIcon" )
	register_event( "DeathMsg", "EventDeath", "a" )
	register_event( "ResetHUD", "PlayerSpawn", "b" )
}

public PlayerSpawn(id) {
	// reset frags quantity
	ProcessDigit( id, .reset = true )
}

public client_disconnect(id) {
	giCurrentKills[id] = 0
}

public EventDeath() {
	new iKiller = read_data(1)
	new iVictim = read_data(2)

	if ( iKiller && is_user_connected(iKiller) && iKiller != iVictim ) {
		if ( giCurrentKills[iKiller] < 9 )	// don't process if limit of 9 is reached
			ProcessDigit( iKiller )
	}
}

ProcessDigit( id, bool:reset = false ) {
	static szSpriteNames[][] = {
		"number_0",
		"number_1",
		"number_2",
		"number_3",
		"number_4",
		"number_5",
		"number_6",
		"number_7",
		"number_8",
		"number_9"
	}

	// set digits color
	static iColor[3] = {
		0,		// red
		160,	// green
		0		// blue
	}

	// hide current digit
	if ( giCurrentKills[id] ) { 	// hiding doesn't needed if there was 0 frags
		message_begin( MSG_ONE_UNRELIABLE, gMsgStatusIcon, _, id )
		write_byte(0) // status: 0 - off, 1 - on, 2 - flash
		write_string( szSpriteNames[ giCurrentKills[id] ] ) // sprite name
		message_end()
	}

	if ( reset ) {
		giCurrentKills[id] = 0
		return
	}

	// show new digit
	message_begin( MSG_ONE_UNRELIABLE, gMsgStatusIcon, _, id )
	write_byte( 1 ) // status: 0 - off, 1 - on, 2 - flash
	write_string( szSpriteNames[ ++giCurrentKills[id] ] ) // sprite name
	write_byte( iColor[0] )
	write_byte( iColor[1] )
	write_byte( iColor[2] )
	message_end()
}