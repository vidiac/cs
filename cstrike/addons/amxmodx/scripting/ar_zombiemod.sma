/*
	Если установлен Zombie Mod и Вы хотите, чтобы при заражении довался опыт, этот плагин для вас.
	Есть одно НО. Этот плагин будет работать, если стоит плагин csstats_mysql.amxx, так как
	дополнительный опыт от заражения будет писаться в Базу Данных MySQL. 
	Без csstats_mysql.amxx, players.ini загадится игроками и сервер начнет лагать и может упасть.
*/

#include <amxmodx>
#include <zombieplague>
#include <army_ranks_ultimate>

#define PLUGIN "AR Zombie Mod"
#define VERSION "14.12.02"
#define AUTHOR "SKAJIbnEJIb"

#define ADDXPPLAYER 3 // Сколько опыта добавить

public plugin_init()
	register_plugin(PLUGIN, VERSION, AUTHOR)

public zp_user_infected_post(id, infector, nemesis) // id - кого заразили | infector - кто заразил
	ar_set_user_addxp(infector, ADDXPPLAYER)