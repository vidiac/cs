��� ������: 62.213.87.70:27005
���� ������ ���������: http://vk.com/wifiserver
#include <amxmodx>
#include <fakemeta>

#define PLUGIN "Snow"
#define VERSION "1.1"
#define AUTHOR "TTuCTOH"

new bool:noMenu[33] = false

new cvar_advert
new cvar_advert_delay
new cvar_menu_delay

public plugin_init()
{	
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
		// Slovarik
	register_dictionary("snow.txt")
		
		// Client commands
	register_clcmd("say /snow", "Snow_Menu")
	register_clcmd("say_team /snow", "Snow_Menu")
		
		// Cvars
	cvar_menu_delay = register_cvar("amx_sm_delay", "2")
	cvar_advert = register_cvar("amx_sm_advert", "1")
	if(get_pcvar_num(cvar_advert))
	{
		cvar_advert_delay = register_cvar("amx_sm_advert_delay", "300")
		new ADVERT_DELAY = get_pcvar_float(cvar_advert_delay)
		set_task(ADVERT_DELAY ,"advert_snow", 0, "",0, "b")
	}
	
		// Death Event
	register_event("DeathMsg", "trololo","a")
}

public plugin_precache()
{
	engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, "env_snow"))
}

public advert_snow()
{
	client_printcolor(0, "%L", LANG_PLAYER, "SNOW_MENU_ADVERT")
}

public Snow_Menu(id)
{
	static MenuHeader[64], MenuOpt1[64], MenuOpt2[64], MenuOpt3[64], MenuOpt4[64], MenuExit[64]
	
	formatex(MenuHeader, charsmax(MenuHeader), "%L", id, "SNOW_MENU_HEADER")
	formatex(MenuOpt1, charsmax(MenuOpt1), "%L", id, "SNOW_MENU_OPT1")
	formatex(MenuOpt2, charsmax(MenuOpt2), "%L", id, "SNOW_MENU_OPT2")
	formatex(MenuOpt3, charsmax(MenuOpt3), "%L", id, "SNOW_MENU_OPT3")
	formatex(MenuOpt4, charsmax(MenuOpt4), "%L", id, "SNOW_MENU_OPT4")
	formatex(MenuExit, charsmax(MenuExit), "%L", id, "SNOW_MENU_EXIT")
	
	new Menu = menu_create(MenuHeader, "Snow_Menu_handler")
	
	menu_additem(Menu, MenuOpt1, "1", 0)
	menu_additem(Menu, MenuOpt2, "2", 0)
	menu_additem(Menu, MenuOpt3, "3", 0)
	menu_additem(Menu, MenuOpt4, "4", 0)
	
	menu_setprop(Menu, MPROP_EXITNAME, MenuExit)
	menu_setprop(Menu, MPROP_EXIT, MEXIT_ALL)
	
	menu_display(id, Menu, 0)
	
	return PLUGIN_CONTINUE
}

public Snow_Menu_handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		
		return PLUGIN_HANDLED
	}
	
	new s_Data[6], s_Name[64], i_Access, i_Callback
	menu_item_getinfo(menu, item, i_Access, s_Data, charsmax(s_Data), s_Name, charsmax(s_Name), i_Callback)
	
	new Key = str_to_num(s_Data)
	
	switch(Key)
	{
		case 1:
		{
			client_cmd(id, "cl_weather 3")
			noMenu[id] = true
			menu_destroy(menu)
			return PLUGIN_HANDLED
		}
		
		case 2:
		{
			client_cmd(id, "cl_weather 2")
			noMenu[id] = true
			menu_destroy(menu)
			return PLUGIN_HANDLED
		}
		case 3:
		{
			client_cmd(id, "cl_weather 1")
			noMenu[id] = true
			menu_destroy(menu)
			return PLUGIN_HANDLED
		}
		case 4:
		{
			client_cmd(id, "cl_weather 0")
			noMenu[id] = true
			menu_destroy(menu)
			return PLUGIN_HANDLED
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public client_connect(id)
{
	client_cmd(id, "cl_weather 3")
	noMenu[id] = false
}

public client_disconnect(id)
{
	noMenu[id] = false
}

public trololo()
{
	new id = read_data(2)
	
	if(noMenu[id])
		return PLUGIN_HANDLED
	
	new MENU_DELAY = get_pcvar_float(cvar_menu_delay)
	set_task(MENU_DELAY, "Snow_Menu", id)
	
	return PLUGIN_CONTINUE
}

// Colorchat
stock client_printcolor ( const id, const input[], any:... )
{
	new iCount = 1, iPlayers[32]
    
	new sNewMsg[191]
	vformat( sNewMsg, charsmax ( sNewMsg ), input, 3 )
    
	replace_all ( sNewMsg, charsmax ( sNewMsg ), "/g", "^4") // green txt
	replace_all ( sNewMsg, charsmax ( sNewMsg ), "/y", "^1") // orange txt
	replace_all ( sNewMsg, charsmax ( sNewMsg ), "/t", "^3") // team txt
    
	if ( id )
		iPlayers[0] = id
	else
		get_players ( iPlayers, iCount, "ch" )
        
	for ( new i; i < iCount; i++ )
	{
		if ( is_user_connected ( iPlayers[i] ) )
		{
			message_begin ( MSG_ONE_UNRELIABLE, get_user_msgid ( "SayText" ), _, iPlayers[i] )
			write_byte ( iPlayers[i])
			write_string ( sNewMsg)
			message_end ()
		}
	}
}