/* chat_rbs Functions
*
* by SKAJIbnEJIb
*
* This file is provided as is (no warranties).
*/

#if defined _chat_rbs_included
  #endinput
#endif
#define _chat_rbs_included


// Какая раскладка стоит у юзера
//	true - русская
//	false - английская
native chat_get_user_translate(id)

// Перевести message[] в русский текст translate[]
native chat_translate_string(message[], translate[], len)