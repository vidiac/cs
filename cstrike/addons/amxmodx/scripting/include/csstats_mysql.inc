/* CsStats MySQL Functions
*
* by SKAJIbnEJIb
*
* This file is provided as is (no warranties).
*/

#if defined _csstats_mysql_included
  #endinput
#endif
#define _csstats_mysql_included_included

////////////////////////
// Константы значений //
////////////////////////
#define FRAGS		0	// Фраги
#define DEATHS		1	// Смерти
#define HEADSHOTS	2	// В голову
#define TEAMKILLS	3	// Убийства своих
#define SHOTS		4	// Выстрелов
#define HITS		5	// Попаданий
#define DAMAGE		6	// Урон
#define SUICIDE		7	// Самоубийства
#define DEFUSING	8	// Начал разминировать бомб
#define DEFUSED		9	// Разминировал бомб
#define PLANTED		10	// Поставил бомб
#define EXPLODE		11	// Взорвал бомб
#define PLACE		12	// Место в статистике
#define LASTTIME	13	// Когда был последний раз (в UNIX времени)
#define GAMETIME	14	// Время в игре (в секундах)
#define CONNECTS	15	// Сыграл игр
#define ROUNDS		16	// Сыграл раундов
#define WINT		17	// Выиграл за Т
#define WINCT		18	// Выиграл за СТ
#define SKILL		19	// Скилл игрока (если запущен плагин statsx_rbs)
#define AR_ADDXP	20	// Добавленный опыт (если запущен плагин army_ranks_ultimate)
#define AR_ANEW		21	// Очки бонусов /anew (если запущен плагин army_ranks_ultimate)

////////////
// Нативы //
////////////

// Запишет название таблицы(где записаны все игроки)
native csstats_TablePlayers(const TablePlayers[], len);


// Возвратит количество игроков в статистике.
native csstats_get_statsnum();

// Получает статистику игрока по id.
// Возвратит:
//	-2	- если игрока нет на серве
//	-1	- если игрок не успел загрузить данные из БД
//	0	- если у игрока STEAM_ID_LAN или что то в этом роде
//	N	- место в статистике
native csstats_get_user_stats(id, stats[22])

// Получает статистику игрока по его месту. 
// write[] - возвратит authid игрока
// Функция возвратит: 1 - удачно, -1 - не подключился к БД
native csstats_get_place_stats(place, stats[22], Name[] = "", len = 0, write[] = "", writelen = 0)

// Возвратит место в статистике
native csstats_get_user_place(id)


// Добавить/отнять что ни будь в статистику игрока
native csstats_add_user_value(id, ident, value)
// Установить нужное значение в пункте в статистики игрока
native csstats_set_user_value(id, ident, value)
// Вернет значение пункта статистики(ident)
native csstats_get_user_value(id, ident)


// Проверяет загрузил ли игрок все данные статистики
native csstats_is_user_connected(id)
// Вернет true, если игрок с нормальным стим айди и т.п.
native csstats_is_user_write(id)


// Преобразует два массива в один
stock stats_to_array(stats1[8], stats2[4], stats[22])
{
	stats[FRAGS]		= stats1[0]
	stats[DEATHS]		= stats1[1]
	stats[HEADSHOTS]	= stats1[2]
	stats[TEAMKILLS]	= stats1[3]
	stats[SHOTS]		= stats1[4]
	stats[HITS]			= stats1[5]
	stats[DAMAGE]		= stats1[6]
	stats[SUICIDE]		= stats1[7]
	stats[DEFUSING]		= stats2[0]
	stats[DEFUSED]		= stats2[1]
	stats[PLANTED]		= stats2[2]
	stats[EXPLODE]		= stats2[3]
}
// Вернет идентификаторы массивов
native Array:csstats_get_array_ident(Trie:array[25])
// Вернет временную статистику
native csstats_get_temp_stats(id, stats[22])
// Очистить пункт во временной статистике
native csstats_clear_temp_stats(id, ident)


// Вызывается, когда плагин подключился к базе и загрузил статистику
forward csstats_initialized_post(Handle:Tuple)

// Вызывается когда игрок зашел на сервер и получил все данные
//	exists вернет true, если игрок уже был в базе, false - если новый игрок
forward csstats_putinserver(id, bool:exists)
