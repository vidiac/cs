/* Army Ranks Ultimate Functions
*
* by SKAJIbnEJIb
*
* This file is provided as is (no warranties).
*/

#if defined _army_ranks_ultimate_included
  #endinput
#endif
#define _army_ranks_ultimate_included

// Возвратит уровень игрока и название звания 
native ar_get_user_level(id, string[] = "", len = 0)

// Возвратит общий опыт игрока.
native ar_get_user_allxp(id)

// Возвратит реальный опыт игрока(без дополнительного)
native ar_get_user_realxp(id)

// Возвратит добавочный опыт игрока
native ar_get_user_addxp(id)

// Возвратит добавочный опыт игрока из players.ini
native ar_get_write_addxp(write[])

// Возвратит количество очков /aNew
native ar_get_user_anew(id)


// Добавить/Отнять реальный опыт игрока(после перезахода игрока, опыт станет прежним)
//	false - в случае неудачи
native ar_set_user_realxp(id, addxp)

// Добавить/Отнять дополнительный опыт игрока
//	false - в случае неудачи
native ar_set_user_addxp(id, addxp)

// Добавить/Отнять количество очков /aNew
//	admin - id админа, который добавляет очки (-1 не писать сообщение)
//	player - id игрока, которому добавляют очки
//	anew - количество добавляемых очков (отрицательное значение отнимет очки)
// Возвратит количество очков игрока. -1 в случаи не удачи
native ar_add_user_anew(admin, player, anew)

// Возвратит данные игрока по его статистике
//	data[0] - опыт
//	data[1] - уровень
//	data[2] - дополнительный опыт
//	data[3] - очки /anew
native ar_get_stats_data(data[4], stats[22])

// Возвратит authid игрока, по записи в статистике
// Сама функция вернет true если успешно. 
// false - если нет игрока или если у него не сгенерировался SteamID(при csstats_rank "1")
native ar_get_user_write(id, write[], len)

// Возвратит максимальное количество уровней.
native ar_get_maxlevels()

// Возвратит название уровня из его номера.
native ar_get_levelname(level, string[], len)

// Возвратит опыт уровня из его номера.
native ar_get_levelxp(level)

// Возвратит 1, если карта есть в списке запрещенных. 0 если нет.
native ar_get_lockmap(const mapname[])

// Возвратит 1, если стоит csdm mode
native ar_get_csdm()

// Возвратит количество HP, которое дали игроку в меню /anew
// Возвратит 0, если не брал этот бонус
native ar_get_bonus_hp(id)

// Перегрузить опыт игрока
native ar_update_player(id)

// Вернет стиль мотд окон
native ar_get_user_style(id, style[], len)

// Вызывается при получении нового уровня.
forward ar_forward_newlevel(id)

// Вызывается при добавлении/убавлении дополнительного опыта
//	num - количество прибавленного/убавленного опыта
forward ar_forward_addxp(id, num)

// Вызывается при добавлении/убавлении очков /aNew
//	num - количество прибавленных/убавленных очков
forward ar_forward_addanew(id, num)
