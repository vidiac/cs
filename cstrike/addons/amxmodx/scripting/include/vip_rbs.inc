/* vip_rbs Functions
*
* by SKAJIbnEJIb
*
* This file is provided as is (no warranties).
*/

#if defined _vip_rbs_included
  #endinput
#endif
#define _vip_rbs_included

// Возвратит 1, если плагин использует золотые оружия
native vip_use_gold()

// Выдаст золотой AK47
native vip_give_user_ak47(id)

// Выдаст золотой M4A1
native vip_give_user_m4a1(id)