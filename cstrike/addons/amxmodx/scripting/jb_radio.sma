﻿#include < amxmodx >
#include < amxmisc >
#include < cstrike >

#define PLUGIN_NAME "[JB] Radio prisoners"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_AUTHOR "OverGame"

#pragma tabsize 0

new const mPrefix[] = "\d[JailBreak]\r"

new sounds[][] = 
{
	"jb_radio/1.mp3",
	"jb_radio/2.mp3",
	"jb_radio/3.mp3",
	"jb_radio/4.mp3",
	"jb_radio/5.mp3",
	"jb_radio/6.mp3",
	"jb_radio/7.mp3",
	"jb_radio/8.mp3",
	"jb_radio/9.mp3"
}

public plugin_init()
{
	register_plugin( PLUGIN_NAME, PLUGIN_VERSION, PLUGIN_AUTHOR )
	register_clcmd( "slot5", "radio1_menu" )
}

public plugin_precache()
{
	for( new i = 0; i <= 9; i++ )
		precache_sound( sounds[i] )
}

public radio1_menu( id )
{
	if ( !is_user_alive( id ) )
		return PLUGIN_HANDLED
		
	if ( cs_get_user_team( id ) == CS_TEAM_CT )
		return PLUGIN_HANDLED
	
	static s_MenuItem[ 255 ]
	formatex( s_MenuItem, charsmax( s_MenuItem ), "%s Пизд*бол это тюрьма!!1^n\yКатегория :\r Радио 1^n\dСтраница ", mPrefix )
	new menu = menu_create(s_MenuItem, "radio1_hand" )
	
	menu_additem(menu, "\yДобивай его", "1")
	menu_additem(menu, "\yМочим их", "2")
	menu_additem(menu, "\yСюда подойди", "3")
	menu_additem(menu, "\yТы че попутал?", "4")
	menu_additem(menu, "\yНадо что-то делать", "5")
	menu_additem(menu, "\yНе подписываемся", "6")
	menu_additem(menu, "\yВалим мусоров", "7")
	menu_additem(menu, "\yОдни обезьяны", "8")
	menu_additem(menu, "\yНаебать решил?", "9")
	
	menu_setprop(menu, MPROP_BACKNAME, "Назад")
	menu_setprop(menu, MPROP_NEXTNAME, "Далее")
	menu_setprop(menu, MPROP_EXITNAME, "Выход")
 
	menu_display( id, menu, 0 )
	return PLUGIN_HANDLED
}

public radio1_hand(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
 
	new data[6], iName[64], access, callback
	menu_item_getinfo(menu, item, access, data, 5, iName, 63, callback)
 
	new key = str_to_num(data)
	new Players[32], Player, Num
	
	get_players(Players, Num, "ch")
	
	for ( new i; i< Num; i++ )
	{
		Player = Players[i]
		
	    if ( cs_get_user_team( Player ) == CS_TEAM_T )
		{
			client_cmd( Player, "mp3 play sound/jb_radio/%d.mp3", key )
		}
	}
	
	return PLUGIN_HANDLED
}

stock ShowMsg(const id, const input[], any:...)
{
	new count = 1, players[32]
	static msg[188]
	vformat(msg, 187, input, 3)
	
	replace_all(msg, 187, "!g", "^4")
	replace_all(msg, 187, "!y", "^1")
	replace_all(msg, 187, "!t", "^3")
	
	if (id) players[0] = id; else get_players(players, count, "ch")
	{
		for (new i = 0; i < count; i++)
		{
			if (is_user_connected(players[i]))
			{
				message_begin(MSG_ONE_UNRELIABLE, get_user_msgid("SayText"), _, players[i])
				write_byte(players[i])
				write_string(msg)
				message_end()
			}
		}
	}
}