----------
Russian

��������� �����:       �������� ��� � ������� ����������� �Radio Terror. RU�  (��� ������ CS 1.3 � ������� )
�������� �����:	       RadioTerrRU_b01.zip
��������:	       ����� �����  ����������� Counter-Strike �� ������� �����
������ �����:	       3,31 MB
����:		       01.05.2004
������:		       0.1 Beta
�����:		       ProvocatoR
E-mail:		       provocator@pisem.net
��������� ������:      http://provocator.pisem.net/main.htm


��������:
--------
��� �������� ����� ����� �����������  ��� ���� Counter-Strike (www.counter-strike.net), ���������������� �� ��������� ����������� ���:

* ���� �Kingpin� (http://planetkingpin.com), �������������� ��������� �7wolf� (www.7wolf.ru); 
* ���� �Kingpin�, �������������� ��������� �Fargus� (www.fargus.com); 
* ���� �Postal 2� (http://www.gopostal.com), �������������� ������ �Akella� (www.akella.com).

�����, � ���� ����������� ����� �� ���� Soldier of Fortune  (http://www.ravensoft.com/soldier.html) � ����� ������ ��������� �����.

��������������:
--------
������������ �������� ������������� �������, ��������� ��������� ���� �����, ���������� ��������, � ����� ����� � ������������ ��������! (� �� �������� �����, ��� � ��� �� ������������ :) )

���������:
--------
1. ����������� ����������, ������������  ��� ���������� � ���������� ����� ������ �����  �radio� (\cstrike\sound\ radio\ ).  ��� �������� ��� ������������ ������������ ������������ � �������.

2. �������������� zip ����� � ���������� \cstrike\. ���,  �� ������ ��������������� ���� �� ��������� ����������, � ����� ����������� ����� "sound"  � ���������� /cstrike/.

��������� �� ����������� ������������ � ���������� ���������, ��� ��� �� ������ ����������� ������� �������� ���������� ������� ��� ������ �� ���.  ��� ����� ��� ����� ������������� �������� ����� � ����������  \cstrike\sound\radio. 
��������: ������������  �ct_coverme�,  ����� ��������������  ������� �ct_coverme2� . ���� �� ������ ������������ ������������ �ct_coverme2�, �� �������� ���������:
1. ������������ ���� �ct_coverme� �� �ct_coverme3� (���, � �������, �ct_covermeOld�)
2. ������������ ����   �ct_coverme2� �� �ct_coverme�.
��� � ��� !

������������:
--------
- ���� �� �����������  ����� �radio� (\cstrike\sound\radio), �� ��� ���������� ����������� ������ ����� � ���������� \cstrike\sound\, ����� ������� ������� ������������ Radio Terror.RU �� ������������. 

- ���� �� ����� ���������� ���� Radio Terror.RU ������������� ����� �radio� (\cstrike\sound\radio), �� ������� �� �������������� �������� �radio�, �������������� ������, ���������� ��� ������������ ����� �radio�, ���������� ������������ Radio Terror.RU

��� ������� �������������:
--------
 - �Valve� - �� ������ ���� ���� ������ � �������!;
 - ���� ���������� �Counter-Strike�;
 - Xatrix - �� ��������  ������������ ���� � �Kingpin�;
- Running With Scissors - �� ������� ����� � ����� �������� ����� �� ���� ���� ��������;
 - ���������� ���� �Soldier of Fortune�; 
 - �������� - ������������� �Fargus�, �7wolf�, �Akella� - �� ���������������� ������� ���;
 - ���� ���� � ���� �� ��, ��� ������� ���� �����  ���������� �������  :-) 

Disclaimer:
--------
�� ������ ����� ������������� � ��������������  ������������ Radio Terror.RU , ������ ���� �� ������������� �������� �� � ���� ��� �����������,  �� �� ������� �� ���� ��������� ��� ��� � �������� � ����. ������� ��� ������� � �������� ���� ;-)

English
---
Tital of file:	 Radio Terror.RU radio messages sounds pack (for CS 1.3 and above)
File Name:	 RadioTerrRU_b01.zip 
Discription:	 New Counter-Strike radio command's  sounds in Russian. 
File Size:	 3,31 MB
Date:		 01.05.2004
Version:	 0.1 beta
Author:		 ProvocatoR
E-mail:		 provocator@pisem.net
Author's Website: http://provocator.pisem.net/main.htm

--------
Discription:
--------
The pack contains new CS radio command's  sounds in russian. All of them where collected from  localized versions of these PC Games:  

* Game Kingpin (http://planetkingpin.com), localized by  7wolf company (www.7wolf.ru)  
* Game Kingpin, localized  by  Fargus company (www.fargus.com) 
* Game Postal 2 (www.gopostal.com), localized by Akella company (www.akella.com)  

Also, pack contains some sounds from  game Soldier of Fortune  (http://www.ravensoft.com/soldier.html) and the voice of the author of this pack. 

WARNING!
--------
Explict content and unnormative lyrics. Children under 18, pregnat women and impressive persons  are not permitted to hear. 

Installation:
--------

1. BACKUP or move your old \sound\radio folder first if you wish to 'rollback'. These files will overwrite your old radio sounds, so remember to back them up.

2. Extract the zip archive to "\cstrike\ folder. Or you could just extract this archive to a temporary directory, and move them into the correct /cstrike/ folder.

Some of the radio commands have  replacements, so that you can choose the most suitable for you.  You can do it by renaming the sound file in the \cstrike\sound\radio folder. Example:
radio message �ct_coverme�  has  replacement �ct_coverme2�  . If you want to use  �ct_coverme2�  do the following:
1. Rename file �ct_coverme� to �ct_coverme3� (or �ct_covermeOld� )
2. Rename file �ct_coverme2� to �ct_coverme�  
There you are ! 

Removal:
--------
IF you have BACKED-UP your \cstrike\sound\radio folder then you may just move the original folder back into your \cstrike\sound\ folder and overwrite  radio sounds with your originals.

Thanks to:
--------
- Valve - for one of the greatest games ever!;
- The creators of Counter-Strike;
- Xatrix - for creating one of my favorite action games � Kingpin;
- Running With Scissors � for cense of humor and for the most bloody shooter  ever;
- The creators  of  Soldier of Fortune game;  
- Companies:  Fargus, 7wolf, Akella � for professional  localizations of the games;
- My Mom & Dad for my wonderful voice :) 

Disclaimer :
--------
If you want to re-edit these, go ahead, but please give me some notification if you are going to use these  sounds in your own radio sounds pack, and give me credit! Thanks.


