INSTALLATION
______________________________________________________________________

Just unzip in your ../cstrike/ directory.
Then launch Counter-Strike and enjoy.


AUTHOR
______________________________________________________________________

Vincent Bailleul alias *aAa* Coucouoeuf (or Ccf)

www.Ccf-mapping.info
www.Team-aAa.com | www.aAa.eu
www.aAa-Solutions.fr

Coucouoeuf@gmail.com


MAP DESCRIPTION
______________________________________________________________________
Name			de_blue
Game			Half-Life
Mod 			Counterstrike
Respawn Points		20+20=40
Status			Final
Size			8 mo
Wad file		included in bsp
models			Palms where already used in de_sunny and made by ginsengavenger.
			The cactus is from de_arizona by Atomfried.


COPYRIGHT INFORMATION
______________________________________________________________________

Textures from		My maps, Nvidia wad, my own photos
Overview		included in .zip file
Sounds from		Counter-Strike original game



OTHER NOTES
______________________________________________________________________

There are 2 easter eggs. You need to be in free look mode to see them.
You can also see a "Ccf" printed on a wall, and all the names of aAa Staff members on some of the doors.



I cannot assume liability if anything goes wrong with your system as
a result of using my map. You're using it at your own risk. 

This map is completly free and cannot be used as a commercial product.



If you distribute this map, you have to include those following files, unaltered :

de_blue.bsp (in /maps)
de_blue.txt (in /maps)
de_blue.bmp (in /overviews)
de_blue.txt (in /overviews)
de_blue_readme.txt
cactus.mdl (in /models)
ginsengavenger_palm240_256.mdl (in /models)
ginsengavenger_palmgroup.mdl (in /models)


Do not modify those files in any way, if you have any question, please e-mail me.


de_blue.bsp is � 2007 by Coucouoeuf [Vincent BAILLEUL]
de_blue is an ESL map.


www.Ccf-mapping.info
www.Team-aAa.com | www.aAa.eu
www.aAa-Solutions.fr